<%-- 
    Document   : product
    Created on : Oct 10, 2023, 11:28:51 PM
    Author     : hieu69
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <section class="product col-md-10">
            <!-- row -->
            <div class="row">
                <div class="col-lg-4 mb-md-5 ">
                    <div class="card h-100">
                        <img src="http://dummyimage.com/400x400.png/dddddd/000000" alt="..." class="card-img-top">
                        <div class="card-body">
                            <div class="text-center">
                                <h5 class="card-title">Fancy Product</h5>
                                $40.00 - $80.00
                            </div>
                        </div>

                        <div class="card-footer  bg-transparent border-top-0">
                            <div class="text-center">
                                <a href="#" class="btn btn-outline-dark">View option</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 mb-md-5 ">
                    <div class="card h-100">
                        <img src="http://dummyimage.com/400x400.png/dddddd/000000" alt="..." class="card-img-top">
                        <div class="card-body">
                            <div class="text-center">
                                <h5 class="card-title">Fancy Product</h5>
                                $40.00 - $80.00
                            </div>
                        </div>

                        <div class="card-footer  bg-transparent border-top-0">
                            <div class="text-center">
                                <a href="#" class="btn btn-outline-dark">View option</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 mb-md-5 ">
                    <div class="card h-100">
                        <img src="http://dummyimage.com/400x400.png/dddddd/000000" alt="..." class="card-img-top">
                        <div class="card-body">
                            <div class="text-center">
                                <h5 class="card-title">Fancy Product</h5>
                                $40.00 - $80.00
                            </div>
                        </div>

                        <div class="card-footer  bg-transparent border-top-0">
                            <div class="text-center">
                                <a href="#" class="btn btn-outline-dark">View option</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 mb-md-5 ">
                    <div class="card h-100">
                        <img src="http://dummyimage.com/400x400.png/dddddd/000000" alt="..." class="card-img-top">
                        <div class="card-body">
                            <div class="text-center">
                                <h5 class="card-title">Fancy Product</h5>
                                $40.00 - $80.00
                            </div>
                        </div>

                        <div class="card-footer  bg-transparent border-top-0">
                            <div class="text-center">
                                <a href="#" class="btn btn-outline-dark">View option</a>
                            </div>
                        </div>
                    </div>
                </div> 

                <div class="col-lg-4 mb-md-5 ">
                    <div class="card h-100">
                        <img src="http://dummyimage.com/400x400.png/dddddd/000000" alt="..." class="card-img-top">
                        <div class="card-body">
                            <div class="text-center">
                                <h5 class="card-title">Fancy Product</h5>
                                $40.00 - $80.00
                            </div>
                        </div>

                        <div class="card-footer  bg-transparent border-top-0">
                            <div class="text-center">
                                <a href="#" class="btn btn-outline-dark">View option</a>
                            </div>
                        </div>
                    </div>
                </div> 

                <div class="col-lg-4 mb-md-5 ">
                    <div class="card h-100">
                        <img src="http://dummyimage.com/400x400.png/dddddd/000000" alt="..." class="card-img-top">
                        <div class="card-body">
                            <div class="text-center">
                                <h5 class="card-title">Fancy Product</h5>
                                $40.00 - $80.00
                            </div>
                        </div>

                        <div class="card-footer  bg-transparent border-top-0">
                            <div class="text-center">
                                <a href="#" class="btn btn-outline-dark">View option</a>
                            </div>
                        </div>
                    </div>
                </div> 

            </div>
        </section>
    </body>
</html>
