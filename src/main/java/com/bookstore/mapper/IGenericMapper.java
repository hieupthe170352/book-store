/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package mapper;

import java.sql.ResultSet;

/**
 *
 * @author hieu69
 */
public interface IGenericMapper<T> {
    
    T mapRow(ResultSet resultSet);
}
