/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.bookstore.mapper.impl;

import com.bookstore.entity.Book;
import java.sql.ResultSet;
import mapper.IGenericMapper;

/**
 *
 * @author hieu69
 */
public class BookMapper implements IGenericMapper<Book>{

    @Override
    public Book mapRow(ResultSet resultSet) {
        try {
            int id = resultSet.getInt("id");
            String name = resultSet.getString("name");
            String desription = resultSet.getString("description");
            String author = resultSet.getString("author");
            float price = resultSet.getFloat("price");
            int quantity = resultSet.getInt("quantity");
            int categoryId = resultSet.getInt("categoryId");
            
            Book book = new Book(id, name, desription, author, price, quantity, name, categoryId);
            return book;
        } catch (Exception e) {
            System.out.println("Wrong at BookMappper: " + e.getMessage());
            e.printStackTrace();
            return null;
        }
    }
    
}
