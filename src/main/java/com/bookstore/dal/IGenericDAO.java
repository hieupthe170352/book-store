/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package dal;

import java.util.List;

/**
 *
 * @author hieu69
 */
public interface IGenericDAO<T> {
    // find all record in table
    // return all record
    List<T> findAll();
    
    
    T findOneById(int id);
}
