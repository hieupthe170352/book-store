/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.bookstore.dal.impl;

import com.bookstore.entity.Book;
import com.bookstore.mapper.impl.BookMapper;
import dal.DBContext;
import dal.IGenericDAO;
import java.util.List;

/**
 *
 * @author hieu69
 */
public class BookDAO extends DBContext<Book> implements IGenericDAO<Book>{

    @Override
    public List<Book> findAll() {
        String sql = "select * from Book";
        List<Book> list = query(sql, new BookMapper());
        return list;
    }

    @Override
    public Book findOneById(int id) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
    
    public static void main(String[] args) {
        BookDAO dao = new BookDAO();
        for(Book book : dao.findAll()) {
            System.out.println(book);
        }
    }
}
